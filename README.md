# Uniform Random Floats

The usual method of generating uniform random floating points by dividing the output of an integer random number generator (RNG) by INT_MAX leads to **very slight biases** in the distribution due to rounding errors. The deviations are so small that it is questionable if it would make any difference. A bigger issue however is that not all valid floats in the range [0,1) are obtained, especially extremely small numbers.

The purpose of this code is to sample **all valid floats in the range [0,1)** with the right frequency so as to produce a uniform distribution. I urge you to understand this clearly. In many cases, the naive method is more than sufficient. See Taylor R Campbell's blog post on the matter [here](http://mumble.net/~campbell/2014/04/28/uniform-random-float).

This is a header only source file, which can be directly included in your C/C++ code. Remember to replace instances of get_random_uint64() / get_random_uint32() in this code with a call to your favourite 64-bit / 32-bit RNG. Make sure that it is okay to use it as a stream of random bits.

## The algorithm

The idea is to generate the fractional part of the float from a random bit-stream, and to get the exponent by counting the leading zeros of another independently drawn random bit-stream. The latter is equivalent to generating a random integer that follows a geometric distribution with p = 0.5. The end result is that all possible floats in the interval [0,1) are sampled properly.

### Warnings

This code depends on hardware / software support for DENORMALs in order to generate a properly uniform distribution down to the smallest number. Also the code will not work on hardware with different endianness / floating point representation. The supplied test program checks if your hardware / software has the properties expected by the code.
