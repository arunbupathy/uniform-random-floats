/*  A simple way to generate all valid floating points in
 *  the range [0,1) with the right frequency so as to get a
 *  uniform distribution in [0,1).
 * 
 *  Remember to replace instances of get_random_uint64() /
 *  get_random_uint32() in the code with a call to your
 *  favourite 64-bit / 32-bit random number generator.
 *  
 *  Tested with gcc version 9.2.1, on linux (kernel 5.3.0, glibc 2.30)
 *  and an x86-64 CPU (AMD Zen uarch v1). May not work on hardware with
 *  different endianness/representation. Also note, without DENORMALs,
 *  the outputs of the algorithm at the smallest exponent will be
 *  overrepresented. So, support for DENORMALs is a requirement.
 *
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

////////////////////////////////////////////////////////////////////////////////

# define num_leading_zeros_64(x) __builtin_clzll(x) // count the number of leading zeros in the given 64bit int

double uniform_random_double(void)
{
    uint64_t rsample = get_random_uint64(); // get a uniformly distributed 64 bit random integer
    uint64_t leading_zeros = (num_leading_zeros_64(rsample >> 52)) - 52; // get the number of leading zeros in the upper 12 bits
    /* ^^ This is the crucial time saving part as we're using the upper 12 bits of the same
     * random integer to get the first guess of the exponent. Chances of it being 12 are slim.
     * NOTE: The actual exponent we need is (1022 - leading zeros), which we do later */
    
    uint64_t max_lz = 12; // maximum number of leading zeros (a counter)
    /* if leading_zeros == max_lz, check if there are more zeros in the random bitstream
     * i.e, generate more random bits to check for leading zeros, if all previous bits were zero */
    while(leading_zeros == max_lz && max_lz < 1022)
    {
        leading_zeros += num_leading_zeros_64(get_random_uint64());
        max_lz += 64;
    }
    uint64_t exponent = (leading_zeros < 1022) ? (1022 - leading_zeros) : 0; // we don't want a -ve exponent
    
    /* As for the DENORMALs i.e., numbers in the range [0, 2^-1022),
     * they will be produced with the same frequency as those
     * in the range [2^-1022, 2^-1021), which is the right thing to do.
     * In fact, the presence of DENORMALs is what makes this method produce
     * a properly uniform distribution down to the smallest number possible. */
    
    rsample &= ((1ULL << 52) - 1); // use the last 52 bits of the first random integer for the fractional part
    rsample |= (exponent << 52); // use (1022 - leading zeros) as the exponent part
    
    double * unirand = (double*) &rsample; // interpret uint64_t as a double!!
    return (*unirand); // return the double
}

////////////////////////////////////////////////////////////////////////////////

# define num_leading_zeros_32(x) __builtin_clz(x) // count the number of leading zeros in the given 64bit int

float uniform_random_float(void)
{
    uint32_t rsample = get_random_uint32(); // get a uniformly distributed 32 bit random integer
    uint32_t leading_zeros = (num_leading_zeros_32(rsample >> 23)) - 23; // get the number of leading zeros in the upper 9 bits
    /* ^^ This is the crucial time saving part as we're using the upper 9 bits of the same
     * random integer to get the first guess of the exponent. Chances of it being 9 are slim.
     * NOTE: The actual exponent we need is (126 - leading zeros), which we do later */
    
    uint32_t max_lz = 9; // maximum number of leading zeros (a counter)
    /* if leading_zeros == max_lz, check if there are more zeros in the random bitstream
     * i.e, generate more random bits to check for leading zeros, if all previous bits were zero */
    while(leading_zeros == max_lz && max_lz < 126)
    {
        leading_zeros += num_leading_zeros_32(get_random_uint32());
        max_lz += 32;
    }
    uint32_t exponent = (leading_zeros < 126) ? (126 - leading_zeros) : 0; // we don't want a -ve exponent
    
    /* As for the DENORMALs i.e., numbers in the range [0, 2^-126),
     * they will be produced with the same frequency as those
     * in the range [2^-126, 2^-125), which is the right thing to do.
     * In fact, the presence of DENORMALs is what makes this method produce
     * a properly uniform distribution down to the smallest number possible. */
    
    rsample &= ((1 << 23) - 1); // use the last 23 bits of the first random integer for the fractional part
    rsample |= (exponent << 23); // use (126 - leading zeros) as the exponent part
    
    float * unirand = (float*) &rsample; // interpret uint32_t as a float!!
    return (*unirand); // return the float
}

////////////////////////////////////////////////////////////////////////////////
