CXX = g++ -Wall
OPFLAGS = -O0
LDFLAGS = -lm

test: test.cpp
	$(CXX) $(OPFLAGS) test.cpp $(LDFLAGS) -o test.o

clean:
	rm -f test.o
