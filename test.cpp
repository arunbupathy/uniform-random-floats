
/*  A simple code to test support for DENORMALs and the representation
 *  expected by the supplied uniform_double() algorithm.
 * 
 * Author: Arunkumar Bupathy
 * email: arunbupathy@gmail.com
 * 
 * This software is distributed under the GNU GPLv3
 * See www.gnu.org for the licence terms */

////////////////////////////////////////////////////////////////////////////////

# include <iostream>
# include <cstdlib>
# include <cmath>

////////////////////////////////////////////////////////////////////////////////

double convert_to_double(uint64_t integer)
{
    double * converted = (double*) &integer; // interpret uint64_t as a double!!
    return (*converted); // return the double
}

////////////////////////////////////////////////////////////////////////////////

float convert_to_float(uint32_t integer)
{
    float * converted = (float*) &integer; // interpret uint32_t as a float!!
    return (*converted); // return the float
}

////////////////////////////////////////////////////////////////////////////////

int main(void)
{
    if((2.0 * convert_to_double(1ULL) == convert_to_double(2ULL)) && (2.0 * convert_to_double(3377699720527872ULL) == convert_to_double(6755399441055744ULL)) && (2.0 * convert_to_double(6755399441055744ULL) != convert_to_double(13510798882111488ULL)) && (2.0 * convert_to_float(1) == convert_to_float(2)) && (2.0 * convert_to_float(6291456) == convert_to_float(12582912)) && (2.0 * convert_to_float(12582912) != convert_to_float(25165824)))
        std::cout << "Test Passed!" << std::endl;
    else
        std::cout << "Test Failed!" << std::endl;
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
